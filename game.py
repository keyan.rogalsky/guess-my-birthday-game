from random import randint

# declaring variables
bYear = randint(1924, 2004)
bMonthNum = randint(1, 12)
guessNum = 6

# takes input from user for name
name = input("Greetings, what is your name? ")

# begin guess process and loop until 5 guesses met
while guessNum <= 5:
    print("Guess", guessNum, ":", name, ", were you born in ",
    bMonthNum, "/", bYear, "?")
    yesNo = input("yes or no? ")
    bYear = randint(1924, 2004)
    bMonthNum = randint(1, 12)

    # checking input
    if yesNo == "yes" or yesNo == "Yes":
        print("Let's gooooooooo")
        print("Easiest guess of my life.")
        exit()
    elif yesNo == "no" or yesNo == "No" and guessNum <= 5:
        print("Drat! Lemme try again.")
        guessNum += 1
    else:
        print("Please answer yes or no.")

print("I've got better things to do, seeya.")

# for loop instead of a while
# issue i'm having is the else using up a guess if
#      the user inputs an invalid answer
# # # guessingNum = [1,2,3,4,5]

# # # for num in guessingNum:
# # #     print("Guess", num, ":", name, ", were you born in ",
# # #     bMonthNum, "/", bYear, "?")
# # #     yesNo = input("yes or no? ")
# # #     bYear = randint(1924, 2004)
# # #     bMonthNum = randint(1, 12)

# # #     # checking input
# # #     if yesNo == "yes" or yesNo == "Yes":
# # #         print("Let's gooooooooo")
# # #         print("Easiest guess of my life.")
# # #         exit()
# # #     elif yesNo == "no" or yesNo == "No":
# # #         print("Drat! Lemme try again.")
# # #     else:
# # #         print("Please answer yes or no.")

# # # print("I've got better things to do, seeya.")
